//+------------------------------------------------------------------+
//|                                         Quebra_Broker Expert.mq5 |
//|                                               Guilherme Ferreira |
//|                                  guilhermeferreira2107@gmail.com |
//+------------------------------------------------------------------+
#property copyright "Guilherme Ferreira"
#property link      "guilhermeferreira2107@gmail.com"
#property version   "1.00"
#property indicator_chart_window
#property indicator_buffers 12
#property indicator_plots   12

//--- plot MA1
#property indicator_label1  "MA1"
#property indicator_type1   DRAW_NONE
#property indicator_color1  clrGray
#property indicator_style1  STYLE_SOLID
#property indicator_width1  2
//--- plot MA2
#property indicator_label2  "MA2"
#property indicator_type2   DRAW_NONE
#property indicator_color2  clrDarkTurquoise
#property indicator_style2  STYLE_SOLID
#property indicator_width2  1
//--- plot MA3
#property indicator_label3  "MA3"
#property indicator_type3   DRAW_NONE
#property indicator_color3  clrGold
#property indicator_style3  STYLE_SOLID
#property indicator_width3  1

//--- plot Compra MA
#property indicator_label4  "CompraMA"
#property indicator_type4   DRAW_ARROW
#property indicator_color4  clrGreen
#property indicator_style4  STYLE_SOLID
#property indicator_width4  3
//--- plot Venda MA
#property indicator_label5  "VendaMA"
#property indicator_type5   DRAW_ARROW
#property indicator_color5  clrViolet
#property indicator_style5  STYLE_SOLID
#property indicator_width5  3

////--- plot SAR
#property indicator_label6  "SAR"
#property indicator_type6   DRAW_NONE
#property indicator_color6  clrBlue
#property indicator_style6  STYLE_SOLID
#property indicator_width6  1

//--- plot Compra SAR
#property indicator_label7  "CompraSAR"
#property indicator_type7   DRAW_ARROW
#property indicator_color7  C'47, 213, 34'
#property indicator_style7  STYLE_SOLID
#property indicator_width7  3
//--- plot Venda SAR
#property indicator_label8  "VendaSAR"
#property indicator_type8   DRAW_ARROW
#property indicator_color8  clrRed
#property indicator_style8  STYLE_SOLID
#property indicator_width8  3

//--- plotar ADX
#property indicator_label9  "ADX"
#property indicator_type9   DRAW_NONE
#property indicator_color9  clrNONE
#property indicator_style9  STYLE_SOLID
#property indicator_width9  0
//--- plotar DI_plus
#property indicator_label10 "DI_plus"
#property indicator_type10  DRAW_NONE
#property indicator_color10 clrNONE
#property indicator_style10 STYLE_SOLID
#property indicator_width10 0
//--- plotar DI_minus
#property indicator_label11 "DI_minus"
#property indicator_type11  DRAW_NONE
#property indicator_color11 clrNONE
#property indicator_style11 STYLE_SOLID
#property indicator_width11 0

////--- plot SAR 2
#property indicator_label12  "SAR_2"
#property indicator_type12   DRAW_NONE
#property indicator_color12  clrBlueViolet
#property indicator_style12  STYLE_SOLID
#property indicator_width12  1

//--- indicator buffers
double         MA1_Buffer[];
double         MA2_Buffer[];
double         MA3_Buffer[];
double         SAR_Buffer[];
double         SAR_2_Buffer[];
double         ADX_Buffer[];
double         DI_plus_Buffer[];
double         DI_minus_Buffer[];

double         CompraMA_Buffer[];
double         VendaMA_Buffer[];
double         CompraSAR_Buffer[];
double         VendaSAR_Buffer[];

//--- parametros
input int MA1_Periodo = 30;
input int MA2_Periodo = 8;
input int MA3_Periodo = 18;
input int SHIFT = 0;
input double   SAR_step=0.05;
input double   SAR_2_step=0.01;
input double   SAR_maximum=0.2;
input int ADX_Periodo = 30;
int DI_plus_Periodo = 0;
int DI_minus_Periodo = 0;
   
   
   
   

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,MA1_Buffer,INDICATOR_DATA);
   SetIndexBuffer(1,MA2_Buffer,INDICATOR_DATA);
   SetIndexBuffer(2,MA3_Buffer,INDICATOR_DATA);
   
   SetIndexBuffer(3,CompraMA_Buffer,INDICATOR_DATA);
   SetIndexBuffer(4,VendaMA_Buffer,INDICATOR_DATA);
   
   SetIndexBuffer(5,SAR_Buffer,INDICATOR_DATA);
   
   SetIndexBuffer(6,CompraSAR_Buffer,INDICATOR_DATA);
   SetIndexBuffer(7,VendaSAR_Buffer,INDICATOR_DATA);
   
   SetIndexBuffer(8,ADX_Buffer,INDICATOR_DATA);
   SetIndexBuffer(9,DI_plus_Buffer,INDICATOR_DATA);
   SetIndexBuffer(10,DI_minus_Buffer,INDICATOR_DATA);
   SetIndexBuffer(11,SAR_2_Buffer,INDICATOR_DATA);
   
//--- setting a code from the Wingdings charset as the property of PLOT_ARROW
   PlotIndexSetInteger(6,PLOT_ARROW,225);
   PlotIndexSetInteger(6,PLOT_ARROW_SHIFT,25);
   PlotIndexSetInteger(7,PLOT_ARROW,226);
   PlotIndexSetInteger(7,PLOT_ARROW_SHIFT,-25);
   
   PlotIndexSetInteger(3,PLOT_ARROW,225);
   PlotIndexSetInteger(3,PLOT_ARROW_SHIFT,25);
   PlotIndexSetInteger(4,PLOT_ARROW,226);
   PlotIndexSetInteger(4,PLOT_ARROW_SHIFT,-25);
   
   
   
     // CONFIGURACAO VISUAL
     //long corAlta = C'218, 247, 166';
     //long corBaixa = C'255, 87, 51';
     long corFUNDO = C'255,255,255';
     long corAlta = C'255, 255, 255';
     long corBaixa = C'0, 0, 0';
     long corLinhaCandle = C'1,1,1';
     long corCandleDoji = C'1,1,1';
     long corForeground = C'36, 113, 163';
     long handle=ChartID();
     
     
     if(handle>0) // Se bem sucedido, customiza adicionalmente
     {
      //--- Desativa auto-rolagem
      ChartSetInteger(handle,CHART_AUTOSCROLL,false);
      //--- Define o encaixe da borda direita do gráfico
      ChartSetInteger(handle,CHART_SHIFT,true);
      //--- Exibe como candles
      ChartSetInteger(handle,CHART_MODE,CHART_CANDLES);
     }
     
     ChartSetInteger(0,CHART_COLOR_BACKGROUND,0,corFUNDO);
     ChartSetInteger(0,CHART_COLOR_CANDLE_BULL,0,corAlta);
     ChartSetInteger(0,CHART_COLOR_CANDLE_BEAR,0,corBaixa);
     ChartSetInteger(0,CHART_COLOR_CHART_UP,0,corLinhaCandle);
     ChartSetInteger(0,CHART_COLOR_CHART_DOWN,0,corLinhaCandle);
     ChartSetInteger(0,CHART_COLOR_CHART_LINE,0,corCandleDoji);
     ChartSetInteger(0,CHART_COLOR_FOREGROUND,0,corForeground);
     ChartSetInteger(0,CHART_SHOW_PERIOD_SEP,0,corForeground);
     
   
   //---
      return(INIT_SUCCEEDED);
   }

void OnDeinit(const int reason)
{
//---
   EventKillTimer();
}

//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---

   // MEDIAS MOVEIS
   CopyBuffer(iMA(_Symbol,_Period,MA1_Periodo,SHIFT,MODE_SMA,PRICE_CLOSE),0,0,rates_total,MA1_Buffer);
   CopyBuffer(iMA(_Symbol,_Period,MA2_Periodo,SHIFT,MODE_SMA,PRICE_CLOSE),0,0,rates_total,MA2_Buffer);
   CopyBuffer(iMA(_Symbol,_Period,MA3_Periodo,SHIFT,MODE_SMA,PRICE_CLOSE),0,0,rates_total,MA3_Buffer);
   
   // SAR PARABOLICO
   CopyBuffer(iSAR(_Symbol,_Period,SAR_step,SAR_maximum),0,0,rates_total,SAR_Buffer);
   CopyBuffer(iSAR(_Symbol,_Period,SAR_2_step,SAR_maximum),0,0,rates_total,SAR_2_Buffer);
      
   //ADX
   CopyBuffer(iADX(_Symbol,_Period,ADX_Periodo),0,0,rates_total,ADX_Buffer);
   
  for(int i=MathMax(1,prev_calculated-1); i<rates_total; i++) {
   
   bool adxEnfraquecendo = ADX_Buffer[i] < ADX_Buffer[i-1] && ADX_Buffer[i-1] < ADX_Buffer[i-2] && ADX_Buffer[i-2] < ADX_Buffer[i-3];
   bool superVelas = tick_volume[i] < (3*tick_volume[i-1]);
   bool adxMin = ADX_Buffer[i] > 25 && ADX_Buffer[i] < 35 ;   
    
   bool candleFechaAnteAbaixoSAR = close[i-1] < SAR_Buffer[i-1];
   bool candleFechaAnteAcimaSAR = close[i-1] > SAR_Buffer[i-1];
   bool candleFechaAcimaSAR = close[i] > SAR_Buffer[i];  
   bool candleFechaAbaixoSAR = close[i] < SAR_Buffer[i];  
   
   bool candleAbreAcimaMA25 = open[i] > MA1_Buffer[i];
   bool candleAbreAbaixoMA25 = open[i] < MA1_Buffer[i];
   bool candleFechaAcimaMA25 = close[i] > MA1_Buffer[i];
   bool candleFechaAbaixoMA25 = close[i] < MA1_Buffer[i];
   
   bool candleAnteAbreAcimaMA25 = open[i-1] > MA1_Buffer[i-1];
   bool candleAnteAbreAbaixoMA25 = open[i-1] < MA1_Buffer[i-1];
   bool candleAnteFechaAcimaMA25 = close[i-1] > MA1_Buffer[i-1];
   bool candleAnteFechaAbaixoMA25 = close[i-1] < MA1_Buffer[i-1];
   
   // INVERSAO DE SAR //
      
   //COMPRA
      if( adxMin && superVelas){
         if(   candleFechaAcimaSAR && 
               candleFechaAnteAbaixoSAR && 
               candleAbreAcimaMA25 &&
               candleFechaAcimaMA25 &&
               candleAnteAbreAcimaMA25 &&
               candleAnteFechaAcimaMA25)
         {
            CompraSAR_Buffer[i] = low[i];
         } else {
            CompraSAR_Buffer[i] = 0;
         }
         
       } else if( ADX_Buffer[i] > 35 ){
            if( close[i] > SAR_2_Buffer[i] && 
                close[i-1] < SAR_2_Buffer[i-1] && 
                open[i] > MA1_Buffer[i] &&
                close[i] > MA1_Buffer[i] &&
                open[i-1] > MA1_Buffer[i-1] &&
                close[i-1] > MA1_Buffer[i-1] )
            {
               CompraSAR_Buffer[i] = low[i];
            } else {
               CompraSAR_Buffer[i] = 0;
            }
        } else {
         CompraSAR_Buffer[i] = 0;
       }    
    
    //VENDA
    if( adxMin && superVelas ){
      if(   candleFechaAbaixoSAR && 
            candleFechaAnteAcimaSAR && 
            candleAbreAbaixoMA25 &&
            candleFechaAbaixoMA25 &&
            candleAnteAbreAbaixoMA25 &&
            candleAnteFechaAbaixoMA25)
      {
         VendaSAR_Buffer[i] = high[i];
      } else {
         VendaSAR_Buffer[i] = 0;
      }
      
    } else if( ADX_Buffer[i] > 35 )
    {
      if( close[i] < SAR_2_Buffer[i] && 
          close[i-1] > SAR_2_Buffer[i-1] && 
          open[i] < MA1_Buffer[i] &&
          close[i] < MA1_Buffer[i] &&
          open[i-1] < MA1_Buffer[i-1] &&
          close[i-1] < MA1_Buffer[i-1] )
      {
            VendaSAR_Buffer[i] = high[i];
      }
      else {
         VendaSAR_Buffer[i] = 0;
      }  
    } else {
      
      VendaSAR_Buffer[i] = 0;
      
    }  
   
   // COMENTARIO AUXILIAR
   if( CompraSAR_Buffer[i] == 0 && VendaSAR_Buffer[i] == 0 ){
      Comment("AGUARDE ORDEM...");
   }
   if( CompraSAR_Buffer[i] > 0){
      Comment("COMPRA. 1 Entrada + 2 martingale.");
   }   
   if( VendaSAR_Buffer[i] > 0){
      Comment("VENDA. 1 Entrada + 2 martingale.");
   }
   
  }
   return(rates_total);
  }
//+------------------------------------------------------------------+


